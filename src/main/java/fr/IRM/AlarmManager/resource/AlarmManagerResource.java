package fr.IRM.AlarmManager.resource;

import java.io.IOException;

import obix.Obj;
import obix.io.ObixDecoder;

import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclispe.om2m.commons.client.Client;
import org.eclispe.om2m.commons.client.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.IRM.AlarmManager.utils.PostUtils;

@RestController
public class AlarmManagerResource {
	
	private Mapper mapper = new Mapper();
	private static String baseUrl = "http://localhost:8080/~/";
	private static String originator = "admin:admin";
	private static String deviceName = "alarm";
	
	//Function to retrieve the last state of the alarm in GEI
	@GetMapping(value="/getAlarmState")
	public String getHeaterState(){
		String res = null;
		Client client = new Client();
		Response resp;		
		try {
			resp = client.retrieve(baseUrl + "in-cse" + "/" + "in-gei" + "/" + deviceName + "/data/la",originator);
			res = resp.getRepresentation();
			String s = resp.getRepresentation();
			ContentInstance cin = (ContentInstance)mapper.unmarshal(s);
			Obj o = ObixDecoder.fromString(cin.getContent());
			res = Integer.toString((int)o.get("Value").getInt());
		} catch(IOException e) {
			e.printStackTrace();
		}

		return res;
	}
	
	//This service allow the customer to set the alarm of the gei (by posting a 1 boolean in the cin)
	@PostMapping(value="/setAlarm")
	public String setAlarm(){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN("in-gei", this.deviceName, 1), baseUrl + "in-cse" + "/" + "in-gei" + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}
	
	//This service allow the customer to reset the alarm of the gei (by posting a 0 boolean in the cin)
	@PostMapping(value="/resetAlarm")
	public String resetAlarm(){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN("in-gei", deviceName, 0), baseUrl + "in-cse" + "/" + "in-gei" + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return res;
	}
}
