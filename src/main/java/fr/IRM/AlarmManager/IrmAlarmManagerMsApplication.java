package fr.IRM.AlarmManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IrmAlarmManagerMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmAlarmManagerMsApplication.class, args);
	}

}
